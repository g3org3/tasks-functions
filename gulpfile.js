const gulp = require('gulp');
const nodemon = require('gulp-nodemon');



// outputs changes to files to the console
function reportChange(event){
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
}

// this task wil watch for changes
// to js, html, and css files and call the
// reportChange method. Also, by depending on the
gulp.task('watch', function() {
  gulp.watch(".", []).on('change', reportChange);
});

gulp.task('test', ['watch'], function () {

    nodemon({ exec: 'npm test',
        ext: 'js',
    })
    .on('start', function () {

        console.log('Starting testing gulp task!');
    });
});
