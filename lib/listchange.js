
module.exports = function(tasks, id_old, id_new) {
	tasks = JSON.parse(JSON.stringify(tasks))
	if ((id_old != id_new) && (id_old > 0) && (id_new > 0) && (id_old <= tasks.length) && (id_new <= tasks.length)) {
		
		var index_old = id_old - 1;
		var index_new = id_new - 1;
		//console.log(tasks);
		var newDate = tasks.slice(index_new, id_new)[0].due_date;

		if (id_new < id_old){
			for (var i = index_new; i < index_old; i++) {
				tasks[i].priority++;
				tasks[i].due_date = tasks[i+1].due_date;
			}
		}
		else {//             5              0
			//console.log(tasks);
			for (var i = index_new; i > index_old; i--) {
				tasks[i].priority--;
				tasks[i].due_date = tasks[i-1].due_date;
				//console.log(i);
				//console.log(tasks[i]);
			}
		}

		var element = tasks.splice(index_old, 1);

		element[0].priority = id_new;
		element[0].due_date = newDate;

		tasks.splice(index_new, 0, element[0]);

	}

    return tasks;
}
