
var assert = require('chai').assert;
var expect = require('chai').expect;
var str = JSON.stringify;

var tasks = [
	{ id: 'A1', name: 'carlos', priority: 1, due_date: '6/20/16' },
	{ id: 'A2', name: 'kenny', priority: 2, due_date: '6/23/16' },
	{ id: 'A3', name: 'erwin', priority: 3, due_date: '6/25/16' },
	{ id: 'A4', name: 'brolo', priority: 4, due_date: '6/27/16' },
	{ id: 'A5', name: 'melanie', priority: 5, due_date: '6/29/16' },
	{ id: 'A6', name: 'andrea', priority: 6, due_date: '6/31/16' }
]
var tasks_sol_5_2 = [
	{ id: 'A1', name: 'carlos', priority: 1, due_date: '6/20/16' },
	{ id: 'A5', name: 'melanie', priority: 2, due_date: '6/23/16' },
	{ id: 'A2', name: 'kenny', priority: 3, due_date: '6/25/16' },
	{ id: 'A3', name: 'erwin', priority: 4, due_date: '6/27/16' },
	{ id: 'A4', name: 'brolo', priority: 5, due_date: '6/29/16' },
	{ id: 'A6', name: 'andrea', priority: 6, due_date: '6/31/16' }
]
var tasks_sol_1_6 = [
	{ id: 'A2', name: 'kenny', priority: 1, due_date: '6/20/16' },
	{ id: 'A3', name: 'erwin', priority: 2, due_date: '6/23/16' },
	{ id: 'A4', name: 'brolo', priority: 3, due_date: '6/25/16' },
	{ id: 'A5', name: 'melanie', priority: 4, due_date: '6/27/16' },
	{ id: 'A6', name: 'andrea', priority: 5, due_date: '6/29/16' },
	{ id: 'A1', name: 'carlos', priority: 6, due_date: '6/31/16' }
]
var tasks_sol_1_3 = [
	{ id: 'A2', name: 'kenny', priority: 1, due_date: '6/20/16' },
	{ id: 'A3', name: 'erwin', priority: 2, due_date: '6/23/16' },
	{ id: 'A1', name: 'carlos', priority: 3, due_date: '6/25/16' },
	{ id: 'A4', name: 'brolo', priority: 4, due_date: '6/27/16' },
	{ id: 'A5', name: 'melanie', priority: 5, due_date: '6/29/16' },
	{ id: 'A6', name: 'andrea', priority: 6, due_date: '6/31/16' }
]
var copytasks = JSON.parse(str(tasks));

/**
 * Tests
 */

describe('List Change', function () {

	describe('#listchange(1, 1)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 1,1)).to.deep.equal(copytasks);
		});
	});

	describe('#listchange(5, 2)', function () {

		it('should return the following array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 5,2)).to.deep.equal(tasks_sol_5_2);
		});
	});

	describe('#listchange(1, 6)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 1,6)).to.deep.equal(tasks_sol_1_6);
		});
	});

	describe('#listchange(1, 3)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 1,3)).to.deep.equal(tasks_sol_1_3);
		});
	});

	describe('#listchange(-1, 2)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, -1,2)).to.deep.equal(copytasks);
		});
	});

	describe('#listchange(1, 7)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 1,7)).to.deep.equal(copytasks);
		});
	});

	describe('#listchange(7, 1)', function () {

		it('should return the same array', function () {

			var listchange = require('../lib').listchange;
			expect(listchange(tasks, 1,7)).to.deep.equal(copytasks);
		});
	});
});
